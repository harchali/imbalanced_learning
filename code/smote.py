#!/usr/bin/python3

## "smote.py": SMOTE algorithm implementation
## Author: Gauthier CASTRO
## University LYON 2 LUMIERE, FRANCE
## NOV 2018

from random import randint
import pandas as pd
from sklearn.datasets import load_iris

from sklearn.utils import shuffle
## matrix input ?
def smote (T, N, k, min_X):
    '''
    SMOTE algorithm
    Input: Number of minority class samples T ; Amount of SMOTE N %; Number of nearest
    neighbors k
    Output: (N/100) * T synthetic minority class samples
    '''
    ## check args type
    if(type(T) is not int): 
        print("T of type: ", type(T), " must be integer")
        return -1
    if(type(N) is not (int or float)):
        print("N of type: ", type(N), " must be integer or float")
        return -1
    if(type(k) is not int):
        print("N of type: ", type(N), " must be integer")
        return -1
    
    ## 1. (∗ If N is less than 100%, randomize the minority class samples as only a random
    ## percent of them will be SMOTEd. ∗)
        
    ## 2. if N < 100
    ## 3.
    ## then Randomize the T minority class samples
    ## 4.
    ## T = (N/100) ∗ T
    ## 5.
    ## N = 100
    ## 6. endif
    if (N<100):
        min_X = shuffle(min_X)
        T = int((N/100) * T)
        N = 100
    

    ## 7. N = (int)(N/100) (∗ The amount of SMOTE is assumed to be in integral multiples of
    ## 100. ∗)
    numattrs = len(min_X.columns)
    newindex = 0
    synthetic = df.DataFrame(columns = min_X.columns)
    ## 12. Synthetic[ ][ ]: array for synthetic samples
    ## (∗ Compute k nearest neighbors for each minority class sample only. ∗)
    ## 13. for i ← 1 to T
    ## 14.
    ## Compute k nearest neighbors for i, and save the indices in the nnarray
    ## 15.
    ## Populate(N , i, nnarray)
    ## 16. endfor

def populate(N, i, nnarray):
    '''
    ## Populate(N, i, nnarray) (∗ Function to generate the synthetic samples. ∗)
    '''
    ## 17. while N  = 0
    while(N >= 0):
    ## 18.
    ## Choose a random number between 1 and k, call it nn. This step chooses one of
    ## the k nearest neighbors of i.

    ## 19.
    ## for attr ← 1 to numattrs
    ## 20.
    ## Compute: dif = Sample[nnarray[nn]][attr] − Sample[i][attr]
    ## 21.
    ## Compute: gap = random number between 0 and 1
    ## 22.
    ## Synthetic[newindex][attr] = Sample[i][attr] + gap ∗ dif
    ## 23.
    ## endfor
    ## 24.
    ## newindex++
    ## 25.
    ## N = N − 1
    ## 26. endwhile
    ## 27. return (∗ End of Populate. ∗)
    return


iris = load_iris()
df = pd.DataFrame(iris.data)
df.columns = iris.feature_names
df["target"] = iris.target_names[iris.target]
df.head()
sdf = shuffle(df)
sdf.head()

